@extends('assets.layout')

@section('content')

	<div class="container">
		@include('assets.header')
	  	@include('assets.filter')
	  	
	  	<div class="bdl-separator">

	  	</div> 
	  @if($latest)
	  
	  <div class="row" id="latest-posts">
	  @foreach($latest as $story)
	   	@include('assets.archive')
	  @endforeach
	  </div>
	  @endif

	</div>
	<div class="container">
		@include('assets.footer')
	</div>
@stop