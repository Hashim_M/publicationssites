@if(!$stories->isEmpty())
	<?php $count = 1; ?>
	@foreach($stories as $pulled_story)
		<?php $story = $pulled_story->story; ?>
		 <div class="item {{ ($count == 1 ? 'active' : '') }}">
              <div class="col-md-4">
                <img title = "{{ $story->feature->feature->name }}" class="img-responsive" src="/img/{{ $story->feature->feature->logo }}">
                <p>
                  {{ $story->feature->feature->description}}
                </p>
                <span>Featured Content</span>
                <a href="/hci/article/{{ $story->slug }}" target="_blank">
                  @if($story->story_type_id == 1)
		  			<img src="/posters/headers/{{ $story->header->filename }}" class="img-responsive">
		  		@elseif($story->story_type_id == 3)
		  			<img src="/posters/videos/{{ $story->video->poster }}" class="img-responsive">
		  		@elseif($story->story_type_id == 2)
		  			<img src="/posters/podcasts/{{ $story->podcast->poster }}" class="img-responsive">
		  		@endif
                  <strong>
                    {{ $story->headline }}
                  </strong>
                </a>
              </div>
            </div>
            <?php $count++ ;?>
	@endforeach
@endif
					