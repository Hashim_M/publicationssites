<div class="col-md-4 story archive-story latest-story">
	<a href="/post/{{ $story['slug'] }}">
	<div class="story-head">
		@if($story['header'])
			<img src="http://bop.boilerhouse.digital/static/headers/{{ $story['header']['filename'] }}" class="img-responsive">
		@endif
		<div class="story-type ">
			@if($story['story_type_id'] == 1)
				<img src="/img/pen_square.png"/>
			@elseif($story['story_type_id'] == 2)
				<img src="/img/podcast_square.png" />
			@elseif($story['story_type_id'] == 3)
				<img src="/img/video_square.png" />
			@endif
		</div>
		@if($story['overlay'])
			<img src="http://bop.boilerhouse.digital/static/overlays/{{ $story['overlay']['filename'] }}" class="img-responsive story-overlay" />
		@endif
	</div>
	</a>
	<h2><a href="/post/{{ $story['slug'] }}">{{ $story['headline'] }}</a></h2>
	<p>{{ $story['standfirst'] }}...<a href="/post/{{ $story['slug'] }}">Read more</a></p>
</div>

