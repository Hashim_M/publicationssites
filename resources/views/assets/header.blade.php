<div class="hci-head">
	<div class="row">
		<div class="col-md-12">
			<div class="pull-right" style="color:#31313F !important; margin-bottom:20px; font-size:16px">
				<a href="#" style="color:#31313F !important;">Contact us  <i class="fa fa-envelope"></i></a> &nbsp; <a href="#" style="color:#31313F !important;">About us <i class="fa fa-question"></i></a>
			</div>
		</div>
	</div>
	<div class="row" style="position:relative">
    <div class="col-md-4">
      <a href = '/'><img src="/img/{{ $site->logo }}" style="margin-bottom:15px" class="img-responsive"></a>
    </div>
    <div class="col-md-5 pull-right col-xs-12 tomn-header">
    	
    	<div class="row">
    		
    		
    		<div class="col-md-3 col-xs-3 col-md-offset-3" style="text-align: center">
    			<a href="/sort/articles" class="hisig-link">
    			<img src="/img/pen_small.png" class="sort-by"  />
    			</a>
    			<br>ARTICLES
    		</div>

    		<div class="col-md-3 col-xs-3" style="text-align: center">
    			<a href="/sort/videos" class="hisig-link">
    			<img src="/img/video_small.png" class="sort-by"  />
    			</a>
    			<br>VIDEOS
    		</div>

    		<div class="col-md-3 col-xs-3" style="text-align: center">
    			<a href="/sort/podcasts" class="hisig-link">
    			<img src="/img/podcast_small.png" class="sort-by" />
    			</a>
    			<br>PODCASTS
    		</div>

    	</div>
  	</div>
	</div>	  	
</div>