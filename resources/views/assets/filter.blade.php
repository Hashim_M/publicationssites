<div class="row hci-filter">
	<div class="col-md-12">
		<div class="filter-wrap">
			<div class="row">
			  	<form role = "form" action="/content/filter" method="GET">
			  		<div class="col-md-5">
			  			<div class="form-group">
		          	<input class="form-control" placeholder="Search by Keywords" name="keywords" id="keywords" value="{{ (isset($form_data['keywords']) ? $form_data['keywords'] : '')}}">
		        	</div>
			  		</div>
			  		<div class="col-md-3">
			  			<div class="form-group">
		          	<select name="timeframe" class="form-control">
		          		<option value="0">All time</option>
		          		<option value="1">Today</option>
		          		<option value="2">Last 7 Days</option>
		          		<option value="3">This Month</option>
		          	</select>
		        	</div>
			  		</div>
			  		<div class="col-md-3">
			  			<div class="form-group">
				          	<select name="feature_id" class="form-control">
				          		<option value="0">Any</option>
				          	</select>
				        </div>
			  		</div>
			  		<div class="col-md-1">
			  			<div class="form-group">
			  				<input type="submit" class="form-control btn btn-danger btn-sm cnm-btn" value="Filter">
				        </div>
			  		</div>
			  	</form>
			</div>
		</div>
	</div>
</div>
