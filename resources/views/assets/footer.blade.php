<div class="footer-light">
  <div class="col-md-4 center">
    <a href="http://www.publicservicedigital.com" target="_blank"><img src="/img/bml/psd.png" /></a>
  </div>
  <div class="col-md-4 center">
    <a href="http://www.councilnewsmonitor.com" target="_blank"><img src="/img/bml/cnm.png"  /></a>
  </div>
  <div class="col-md-4 center">
    <a href="http://www.theinformationdaily.com" target="_blank"><img src="/img/bml/infodaily.png" /></a>
  </div>
  <div class="col-md-12">
    <h2>TOGETHER WE'VE GOT THE PUBLIC SECTOR COVERED</h2>
  </div>
</div>
<div class="footer-dark">
  <div class="col-md-4 copyright">
        <img src="/img/{{ $site->footer }}" class="img-responsive">
        <p>
   {{ $site->site_name }} is owned by Boilerhouse Online Publications, and is associated with our other titles and platforms including
            @if($site->id == 1) Council News Monitor and Public Service Digital.
            @elseif($site->id == 2) Council News Monitor and Information Daily.
            @elseif($site->id == 3) Information Daily and Public Service Digital.@endif</p>
      
  </div>
  <div class="col-md-4 copyright" >
    <h2>Contact</h2>
    <p>
      <i class="fa fa-building-o"></i>  Walker Building, 58 Oxford Street, Birmingham B5 5NR<br>
      <i class="fa fa-phone"></i>  0845 094 5641<br>
      <i class="fa fa-envelope-o"></i>  info@boilerhousepublications.co.uk<br>
    </p>
  </div>
  <div class="col-md-4 copyright">
   <h2>&nbsp;</h2>
   <p><a href="#">Privacy & Cookies</a> | <a href="#">Terms & Conditions</a></p>
   <p>&copy; {{ date('Y') }} Boilerhouse Online Publications<br>
   Design by <a href = "#">Boilerhouse Creative</a>
   </p>
  </div>
</div>
