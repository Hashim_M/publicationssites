@extends('assets.layout')

@section('content')

	<div class="container">
		@include('assets.header')
	  	@include('assets.filter')
	  	
	  	<div class="bdl-separator">

	  	</div> 
	  @if($latest)
	  
	  <div class="row" id="latest-posts">
	  @foreach($latest as $story)
	   	@include('assets.latest')
	  @endforeach
	  </div>
	  <div class="row" id="archive-posts">
	  @foreach($archive as $story)
	   	@include('assets.archive')
	  @endforeach
	  </div>
	  <div class="row">
	  	<div class="col-md-12">
	  		<a href ="#" class="btn btn-block load-more" data-offset = "14" data-limit = "8" data-type = "{{ $type_id }}">load More</a>
	  	</div>
	  </div>
	  @endif

	</div>
	<div class="container">
		@include('assets.footer')
	</div>
@stop