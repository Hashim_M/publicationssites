@extends('assets.layout')

@section('content')

	<div class="container">
		@include('assets.header')	
		@include('assets.filter')
	  <div class="row">
	  	<div class="col-md-3">
	  		<div class="bdl-separator"></div>
	  		<div class="story-type-side">
	  			<div class="story-type">
		  			@if($story['story_type_id'] == 1)
		  				<img src="/img/nib_pen.png" width="60" height="60" />
		  			@endif
		  		</div>
		  		<span class = "story-back"><a href="/">BACK TO<br>FRONT PAGE</a></span>
	  		</div>
	  		<h3 class="story-author">{{ $story['author']['first_name'] }} {{ $story['author']['last_name'] }}</h3>
	  		<h5 class="story-published">{{ date('l, F j, Y',strtotime($story['published_at'])) }} <br> {{ date('H:m',strtotime($story['published_at'])) }} GMT</h5>
	  		<p class="story-bio">{{ $story['author']['bio'] }}</p>
	  		@if($story['author']['twitter'] != "")
	  			<a href="https://twitter.com/{{ $story['author']['twitter'] }}" target="_blank" class="story-twitter-bio"><i class="fa fa-twitter"></i> {{ $story['author']['twitter'] }}</a>
	  		@endif
	  		<div class="sponsor-block">
	  			@if($story['advert'])
	  				<h3 class="sidebar-head">Content Sponsor</h3>
	  				<a href="{{ $story['advert']['ad_url'] }}" target="_blank"><img src="//bop.boilerhouse.digital/static/adverts/{{ $story['advert']['filename'] }}" class="img-responsive" /></a>
	  				
	  			@endif
	  		</div>
	  		
	  	</div>
	  	<div class="col-md-9 story-full">
	  		<div class="bdl-separator"></div>
	  		<h2>{{ $story['headline'] }}</h2>
	  		<div style="width: 100%; height: 100%; overflow: hidden;">
	  			<div class="col-md-10 col-sm-12" style="position:relative; padding-left: 0px; padding-right: 0px;">
		  		@if($story['story_type_id'] == 1)
			  			<img src="http://bop.boilerhouse.digital/static/headers/{{ $story['header']['filename'] }}" class="img-responsive">
			  			@if($story['overlay'])
		  					<img src="http://bop.boilerhouse.digital/static/overlays/{{ $story['overlay']['filename'] }}" class="img-responsive story-overlay" />
		  				@endif
		  		@elseif($story['story_type_id'] == 3)
		  			<iframe src="//player.vimeo.com/video/<?php echo str_replace('https://vimeo.com/','',$story['video_url']);?>" width="735" height="290" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		  		@elseif($story['story_type_id'] == 2)
		  			<?php echo $story['podcast_url']; ?>
		  		@endif
		  		</div>
		  		<div class="col-md-2 col-sm-12 header-wrapper {{ ($story['story_type_id'] == 2 ? "header-audio" : "") }}">
		  			<span style="display: block">
		  				<h3>SHARE</h3>
		  				<div class="social-block">
		  		<span class='st_facebook_large sep' displayText='Facebook'></span>
				<span class='st_twitter_large sep' displayText='Tweet' st_via = "TheInfoDaily"></span>
				<span class='st_linkedin_large sep' displayText='LinkedIn'></span>
				<span class='st_email_large sep' displayText='Email'></span>
			</div>
		  			</span>
		  			<span style="display: block; position: absolute; bottom: 5px;">
		  			@if($story['header']['copyright'] != '')
		  				<strong>Image:</strong> <br>{{ $story['header']['copyright'] }}
		  			@endif
		  			</span>
		  		</div>
	  		</div>
	  		<p class="standfirst-p">
	  		{{ $story['standfirst'] }}
	  		</p>
	  		
	  		{!! html_entity_decode($story['content']) !!}
	  		<p>&nbsp;</p>
	  		<p><a href = "/">Back to homepage</a></p>
	  	</div>
	  </div>
	 </div>
	 <div class="container">
		@include('assets.footer')
	</div>
@stop