<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'SiteController@home');
Route::get('/content/filter', ['as' => '/filter', 'uses' => 'SiteController@filter']);

Route::get('/post/{slug}', 'SiteController@getPost');
Route::get('/content/{offset}/{limit}/{type_id}', 'SiteController@getContent');
Route::get('/sort/{type}', 'SiteController@getSort');

//Route::get('/admin', 'Admin\AdminController@getIndex');
//Route::post('/admin/login', 'Admin\AdminController@postLogin');
//Route::get('/admin/logout', 'Admin\AdminController@getLogout');

