$(".chosen-select").chosen();

$('.edit-author').click(function() {
	var author_id = $(this).data("id");
	var modal = $('#edit');
	 $.getJSON('/admin/authors/author/'+author_id, function(data) {
        $('input[name=name]', modal).val(data.name);
        $('input[name=bio]', modal).val(data.bio);
        $('input[name=twitter]', modal).val(data.twitter);
        $('input[name=author_id]', modal).val(data.id);
        
        modal.modal('show');
    });
	 return false;
});

$('.related-story').select2({
    minimumInputLength: 2,
    tags: [],
    allowClear: true,
    placeholder: "Search for Related Story",
    ajax: {
        url: '/admin/stories/search',
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (term) {
            return {
                term: term
            };
        },
        processResults: function (data) {
	      // parse the results into the format expected by Select2.
	      // since we are using custom formatting functions we do not need to
	      // alter the remote JSON data
	      return {
	        results: $.map(data, function (item) {
	        	return {
		           id: item.id,
		           text: item.value
		        };
	        })
	      };
	    },
	    cache: true
    }
});


$('.media-browser').click(function() {
	var media = $(this).data("media");
	$('#media-browser-body').html('');
	switch(media) {
		case "header":
			var selected = $('#header_id').val();
			$.get('/admin/headers/headers', function(data) {
				$.each(data, function(key,value) {
					var selected_html = '';
					if(selected == value.id)
						selected_html = 'selected-media';
					var html = '<div class = "col-md-4">';
					html += '<div class = "media-item '+selected_html+'" data-media = "header" data-item = "'+value.id+'"><img class = "img-responsive" style = "height:120px" src = "/posters/headers/';
					html += value.filename+'" />'+value.title+'</div></div>';
					$('#media-browser-body').append(html);
				});
			});
		break;
		case "video":
			var selected = $('#video_id').val();
			$.get('/admin/videos/videos', function(data) {
				$.each(data, function(key,value) {
					var selected_html = '';
					if(selected == value.id)
						selected_html = 'selected-media';
					var html = '<div class = "col-md-4" >';
					html += '<div class = "media-item '+selected_html+'"  data-media = "video" data-item = "'+value.id+'"><img class = "img-responsive" style = "height:120px" src = "/posters/videos/';
					html += value.poster+'" />'+value.title+'</div></div>';
					$('#media-browser-body').append(html);
				});
			});
		break;
		case "podcast":
			var selected = $('#podcast_id').val();
			$.get('/admin/podcasts/podcasts', function(data) {
				$.each(data, function(key,value) {
					var selected_html = '';
					if(selected == value.id)
						selected_html = 'selected-media';
					var html = '<div class = "col-md-4">';
					html += '<div class = "media-item '+selected_html+'" data-media = "podcast" data-item = "'+value.id+'"><img class = "img-responsive" style = "height:120px" src = "/posters/podcasts/';
					html += value.poster+'" />'+value.title+'</div></div>';
					$('#media-browser-body').append(html);
				});
			});
		break;
	}
	
	$('#media-browser').modal('show');
	
});

$(document).on('click','.media-item', function() {
	$('div.selected-media').removeClass('selected-media');
	$(this).addClass('selected-media');
	
	var media = $(this).data("media");
	var item = $(this).data("item");

	switch(media) {
		case "header":
			$('#header_id').val(item);
		
		break;
		case "video":
			$('#video_id').val(item);
			
		break;
		case "podcast":
			$('#podcast_id').val(item);
			
		break;
	}
});

function countChar(obj,obj_id) {
	var chars = obj.value.length;
	$('#'+obj_id).html(chars+' characters');

}