$(function(){
  var navbar_height = $('nav.navbar').height();

  // scroll spy offset
  $('body').data('offset', navbar_height + 20);

  $(document).on('click','.load-more',function() {
    var offset = $(this).data('offset');
    var limit = $(this).data('limit');
    var type_id = $(this).data('type');
    var new_offset = offset + limit;
    $(this).data('offset',new_offset);
    $.get('/content/'+offset+'/'+limit+'/'+type_id, function(data) {
      $('#archive-posts').append(data);
    });
    
    return false;
  });

  // scrollTo functionality
  $("#subnavbar-ockham ul li a[href^='#']").on('click', function(e) {

    // prevent default anchor click behavior
    e.preventDefault();

    // store hash
    var hash = this.hash;

    // animate
    $('html, body').animate({
      scrollTop: ($(hash).offset().top - navbar_height)
    }, 1000, function(){
      // when done, add hash to url
      // (default click behaviour)
      window.location.hash = hash;
    });

  });

  $('.subnav-ockham').on({
    "click":function(e){
      e.stopPropagation();
    }
  });

  $('.infinite-scroll').jscroll({
    nextSelector: 'a.scroll-next:last', autoTrigger: false
  });

  $('video,audio').mediaelementplayer();

  // only load 3 if the screen is wide enough (otherwise they'll stack)
  // otherwise default to the standard 1 item carousel.
  if (document.documentElement.clientWidth > 768) {
    // Snippet for multiple items: http://www.bootply.com/94444
    $('#him .carousel .item').each(function(){
      var next = $(this).next();

      if (!next.length) {
        next = $(this).siblings(':first');
      }

      next.children(':first-child')
        .clone()
        .appendTo($(this));

      if (next.next().length > 0) {
        next.next()
          .children(':first-child')
          .clone()
          .appendTo($(this));
      }
      else {
        $(this).siblings(':first')
          .children(':first-child')
          .clone()
          .appendTo($(this));
      }
    });
  }
});
