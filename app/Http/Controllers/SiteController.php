<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Site;

class SiteController extends Controller
{

    public $base_url = "http://bop.boilerhouse.digital";


    public $site;

    public function __construct() {
        $this->site = $this->getSite();
    }

    private function getSite() {
        $site_server_name = $_SERVER['SERVER_NAME'];
        $site_server_name = str_replace("www.","",$site_server_name);
        $site = Site::where('server_name',$site_server_name)->first();
        return $site;
    }

    private function fetchLatest($offset = 0, $limit = 6, $type = false) {
        if(!$type)
            $res = file_get_contents($this->base_url."/fetch/latest/".$this->site->id."/$offset/$limit");
        else
            $res = file_get_contents($this->base_url."/fetch/latest/".$this->site->id."/$offset/$limit/$type");
        return json_decode($res,true);
    }

    private function fetchStory($slug) {

        $res = file_get_contents($this->base_url."/fetch/story/".$slug."/".$this->site->id);
        return json_decode($res,true);
    }

    private function fetchSearch($form_data) {

        $res = file_get_contents($this->base_url."/search/".$this->site->id."/".$form_data['keywords']."/".$form_data['timeframe']);
        return json_decode($res,true);
    }

    public function home() {

        $features   = [];
        $latest = $this->fetchLatest();
        $archive = $this->fetchLatest(6,8);

            return view('home')
                ->with('features', $features)
                ->with('latest', $latest)
                ->with('type_id', 0)
                ->with('site',$this->getSite())
                ->with('archive', $archive);
    }

    public function getSort($type) {
        $types = ['articles' => 1, 'podcasts' => 2, 'videos' => 3];
        $features   = [];
        $latest = $this->fetchLatest(0,6,$types[$type]);
        $archive = $this->fetchLatest(6,8,$types[$type]);

            return view('home')
                ->with('features', $features)
                ->with('latest', $latest)
                ->with('type_id', $types[$type])
                ->with('site',$this->getSite())
                ->with('archive', $archive);

    }
    public function getScrollArchive($page = false) {

        if(!$page)
            $page = 1;
        $offset = (($page-1) * 8) + 17;
        $page = (int)$page;
        $next_page = $page + 1;
        $sections['archive'] = Story::whereNotNull('published')->orderBy('published','DESC')->take(8)->offset($offset)->get();


          return view('website.archive')
                ->with('sections', $sections)
                ->with('site',$this->getSite())
                ->with('next_page', $next_page);

    }

    public function getPost($slug = false) {

        $active_tab = "hci";

        if($slug) {
            $story = $this->fetchStory($slug);
            $features   = [];

            if($story) {
                    return view('story')
                        ->with('story', $story)
                        ->with('title', $story['headline'])
                        ->with('active_tab', $active_tab)
                        ->with('site',$this->getSite())
                        ->with('features', $features);
            }
            else
                abort(404);
        }

    }

    public function getContent($offset,$limit, $type_id) {
        $posts = $this->fetchLatest($offset,$limit,$type_id);
        $content = "";

        foreach($posts as $story) {
                $content .= view('assets.archive', compact('story'))->render();

        }
        echo $content;
    }



    public function filter() {
        $form_data = \Request::all();
        if(!$form_data['keywords'] || $form_data['keywords'] == "")
            $form_data['keywords'] = 'blank';

        $latest = $this->fetchSearch($form_data);
        $features   = [];


            return view('results')
                ->with('features', $features)
                ->with('site',$this->getSite())
                ->with('latest', $latest);
        }

}
