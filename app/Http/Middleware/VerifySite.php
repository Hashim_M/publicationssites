<?php

namespace App\Http\Middleware;

use Closure;
use App\Sites;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class VerifySite
{


    public $site;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        $site_server_name = $_SERVER['SERVER_NAME'];
        $site = Sites::where('server_name', $site_server_name)->first();

        if(!$site) {

            throw new NotFoundHttpException();
        }
        
        $this->site = $site;
        
        return $next($request);
    }


    /**
     * @return $this->site
     */
    public function getSite() {
        var_dump($this->site); die();
        return $this->site;
    }
}
